// ==UserScript==
// @name         gitlab-ux-userscript
// @namespace    https://gitlab.com/sevcik.tk/gitlab-ux-userscript
// @downloadURL  https://gitlab.com/sevcik.tk/gitlab-ux-userscript/raw/master/gitlab-ux-userscript.user.js
// @version      0.1
// @description  GitLab UX
// @author       Pavel Sevcik
// @include      https://gitlab.com/*/issues*
// @include      https://gitlab.com/*/INBOX
// @include      https://gitlab.com/*/OKR
// @include      https://gitlab.com/*/OPS
// @run-at       document-end
// @grant        none
// @license      MIT
// ==/UserScript==

(function() {
  'use strict';

  if (['INBOX', 'OKR', 'OPS'].includes(document.location.href.split('/').pop())) {
      window.location = `${document.location.href}/issues`;
      return;
  }

  let navlinks = document.querySelector('.nav-links.issues-state-filters');
  if (navlinks) {
      let myallnavlink = document.createElement('li');
      let mystartednavlink = document.createElement('li');
      let myallparams = [
          '?',
          'assignee_username=', gon.current_username,
          '&scope=', 'all',
          '&state=', 'opened'
      ].join('');
      myallnavlink.innerHTML = `<li class=""><a id="state-myall" title="Show all my issues." data-state="myall" href="${myallparams}"><span>My</span></a></li>`;
      let mystartedparams = [
          '?',
          'assignee_username=', gon.current_username,
          '&milestone_title=', '%23started',
          '&scope=', 'all',
          '&sort=', 'milestone_due_asc',
          '&state=', 'opened'
      ].join('');
      mystartednavlink.innerHTML = `<li class=""><a id="state-my" title="Show my issues." data-state="my" href="${mystartedparams}"><span>Started</span></a></li>`;
      navlinks.appendChild(mystartednavlink);
      navlinks.appendChild(myallnavlink);
  }

  let discusionOptionButton = document.querySelector('button.qa-discussion-option');
  discusionOptionButton && discusionOptionButton.click();
})();